"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.removeColumn("Departments", "id");
    await queryInterface.removeColumn("Courses", "department_id");
    await queryInterface.removeColumn("Students", "department_id");
    await queryInterface.removeColumn("Instructors", "department_id");
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.addColumn("Departments", "id", {
      allowNull: false,
      type: Sequelize.INTEGER,
      autoIncrement: true,
    });
    await queryInterface.addColumn("Courses", "department_id", {
      allowNull: false,
      type: Sequelize.INTEGER,
    });
    await queryInterface.addColumn("Students", "department_id", {
      allowNull: false,
      type: Sequelize.INTEGER,
    });
    await queryInterface.addColumn("Instructors", "department_id", {
      allowNull: false,
      type: Sequelize.INTEGER,
    });
  },
};
