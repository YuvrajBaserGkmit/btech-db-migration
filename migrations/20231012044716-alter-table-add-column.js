"use strict";

const { DataTypes } = require("sequelize");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("Courses", "department_id", {
      type: Sequelize.UUID,
      allowNull: false,
    });
    await queryInterface.addColumn("Students", "department_id", {
      type: Sequelize.UUID,
      allowNull: false,
    });
    await queryInterface.addColumn("Instructors", "department_id", {
      type: DataTypes.UUID,
      allowNull: false,
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn("Courses", "department_id");
    await queryInterface.removeColumn("Students", "department_id");
    await queryInterface.removeColumn("Instructors", "department_id");
  },
};
