"use strict";

const student = require("../models/student");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addColumn("Students", "city", {
      type: Sequelize.STRING,
      allowNull: true,
    });
    await queryInterface.addColumn("Instructors", "city", {
      type: Sequelize.STRING,
      allowNull: true,
    });
    await queryInterface.addColumn("Departments", "nid", {
      allowNull: false,
      type: Sequelize.UUID,
      defaultValue: Sequelize.literal("uuid_generate_v4()"),
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeColumn("Students", "city");
    await queryInterface.removeColumn("Instructors", "city");
    await queryInterface.removeColumn("Departments", "nid");
  },
};
