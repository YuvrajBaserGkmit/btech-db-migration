"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Students", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      first_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      last_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      date_of_birth: {
        allowNull: false,
        type: Sequelize.DATEONLY,
        validate: {
          isDate: true,
          min: 1,
          max: 120,
        },
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
        validate: {
          isEmail: true,
        },
      },
      phone_number: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
        validate: {
          isNumeric: true,
        },
      },
      interests: {
        allowNull: true,
        type: Sequelize.ARRAY(Sequelize.STRING),
      },
      grades: {
        allowNull: true,
        type: Sequelize.JSON,
      },
      department_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          // This is a reference to another model
          model: "Departments",

          // This is the column name of the referenced model
          key: "id",
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Date.now(),
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Date.now(),
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: null,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Students");
  },
};
