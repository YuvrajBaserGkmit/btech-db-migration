"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.addConstraint("Courses", {
      fields: ["department_id"],
      type: "foreign key",
      name: "Courses_department_id_fkey",
      references: {
        table: "Departments",
        fields: ["id"],
      },
    });
    await queryInterface.addConstraint("Students", {
      fields: ["department_id"],
      type: "foreign key",
      name: "Students_department_id_fkey",
      references: {
        table: "Departments",
        fields: ["id"],
      },
    });
    await queryInterface.addConstraint("Instructors", {
      fields: ["department_id"],
      type: "foreign key",
      name: "Instructors_department_id_fkey",
      references: {
        table: "Departments",
        fields: ["id"],
      },
    });
    await queryInterface.addConstraint("instructors_courses", {
      fields: ["course_id"],
      type: "foreign key",
      name: "instructors_courses_course_id_fkey",
      references: {
        table: "Courses",
        fields: ["id"],
      },
    });
    await queryInterface.addConstraint("instructors_courses", {
      fields: ["instructor_id"],
      type: "foreign key",
      name: "instructors_courses_instructor_id_fkey",
      references: {
        table: "Instructors",
        fields: ["id"],
      },
    });
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.removeConstraint(
      "Courses",
      "Courses_department_id_fkey"
    );
    await queryInterface.removeConstraint(
      "Students",
      "Students_department_id_fkey"
    );
    await queryInterface.removeConstraint(
      "Instructors",
      "Instructors_department_id_fkey"
    );
    await queryInterface.removeConstraint(
      "instructors_courses",
      "instructors_courses_course_id_fkey"
    );
    await queryInterface.removeConstraint(
      "instructors_courses",
      "instructors_courses_instructor_id_fkey"
    );
  },
};
