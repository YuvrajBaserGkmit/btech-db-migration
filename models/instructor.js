"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Instructor extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.Instructor, {
        through: models.instructors_courses,
        foreignKey: "instructor_id",
      });
      this.belongsTo(models.Department, {
        foreignKey: "department_id",
      });
    }
  }
  Instructor.init(
    {
      instructor_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      department_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          // This is a reference to another model
          model: "Departments",

          // This is the column name of the referenced model
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "Instructor",
      underscored: true,
      paranoid: true,
    }
  );
  return Instructor;
};
