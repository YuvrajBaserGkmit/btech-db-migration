"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class instructors_courses extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  instructors_courses.init(
    {
      course_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          // This is a reference to another model
          model: "Courses",

          // This is the column name of the referenced model
          key: "id",
        },
      },
      instructor_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          // This is a reference to another model
          model: "Instructors",

          // This is the column name of the referenced model
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "instructors_courses",
      underscored: true,
      paranoid: true,
    }
  );
  return instructors_courses;
};
