"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Student extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Department, {
        foreignKey: "department_id",
      });
    }
  }
  Student.init(
    {
      first_name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      last_name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      date_of_birth: {
        allowNull: false,
        type: DataTypes.DATEONLY,
        validate: {
          isDate: true,
          min: 1,
          max: 120,
        },
      },
      email: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true,
        validate: {
          isEmail: true,
        },
      },
      phone_number: {
        allowNull: false,
        type: DataTypes.STRING,
        unique: true,
        validate: {
          isNumeric: true,
        },
      },
      interests: {
        allowNull: true,
        type: DataTypes.ARRAY(DataTypes.STRING),
      },
      grades: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      department_id: {
        allowNull: false,
        type: DataTypes.INTEGER,
        references: {
          // This is a reference to another model
          model: "Departments",

          // This is the column name of the referenced model
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "Student",
      underscored: true,
      paranoid: true,
    }
  );
  return Student;
};
