"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Course extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsToMany(models.Instructor, {
        through: models.instructors_courses,
        foreignKey: "course_id",
      });
      this.belongsTo(models.Department, {
        foreignKey: "department_id",
      });
    }
  }
  Course.init(
    {
      course_name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      course_description: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      department_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          // This is a reference to another model
          model: "Departments",

          // This is the column name of the referenced model
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "Course",
      underscored: true,
      paranoid: true,
    }
  );
  return Course;
};
